# HomeAssistant

## Getting started

Adding Tuya cloud to your integration, follow [this guide](https://developer.tuya.com/en/docs/iot/Home-assistant-tuya-intergration?id=Kb0eqjig0utdd)

> Country	Select the region of your account of the Smart Life app.
> Note: Open the mobile app and tap Me > Setting > Account and Security > Region.
> Tuya IoT Access ID and Tuya IoT Access Secret	
    - Go to the Tuya IoT Development Platform and select your cloud project. Click the Overview tab and find the Access ID and Access Secret in the Authorization Key area.
- Access ID/Client ID: Tuya IoT Access ID 
- Access Secret/Client Secret: Tuya IoT Access Secret
- Account:	Your account of the Smart Life app.
- Password:	Your password for the Smart Life app.

[Ref](https://developer.tuya.com/en/docs/iot/Home-assistant-tuya-intergration?id=Kb0eqjig0utdd)